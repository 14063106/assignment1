
public class Classes {

	private String id;
	private String type;
	private String roomNumber;
	private int timing;
	private int studentNumber;
	private String week;
	
	public Classes(String id, String type, String roomNumber, int timing, int studentNumber, String week) {
		super();
		this.id = id;
		this.type = type;
		this.roomNumber = roomNumber;
		this.timing = timing;
		this.studentNumber = studentNumber;
		this.week = week;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getTiming() {
		return timing;
	}

	public void setTiming(int timing) {
		this.timing = timing;
	}

	public int getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(int studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}
	
	
	
	
}
