import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class LoadingData {
	
	public static void main(String[] args)
	{}
	
	public ArrayList<TeachingStaff> teachingStaff()
	{
		ArrayList<TeachingStaff> list = new ArrayList<>();
		try
		{
			File file = new File("src\\teaching.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while(line != null)
			{
				String[] data = line.split(",");
				TeachingStaff staff = new TeachingStaff(data[0],data[1],data[2],data[3],data[4],data[5]);
				list.add(staff);
				line = br.readLine();
			}
			
		}
		catch(Exception ex)
		{}
		
		return list;
	}
	
	public ArrayList<Classes> classes()
	{
		ArrayList<Classes> list = new ArrayList<>();
		try
		{
			File file = new File("src\\classes.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while(line != null)
			{
				String[] data = line.split(",");
				Classes cls = new Classes(data[0], data[1], data[2], Integer.parseInt(data[3]), Integer.parseInt(data[4]), data[5]);
				list.add(cls);
				line = br.readLine();
			}
			
		}
		catch(Exception ex)
		{}
		
		return list;
	}

}
