import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.junit.Test;

public class Testing {

	@Test
	public void testTeachingStaff() {
		LoadingData loading  = new LoadingData();
		ArrayList<TeachingStaff> input = new ArrayList<>();
		try
		{
			File file = new File("src\\teaching.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while(line != null)
			{
				String[] data = line.split(",");
				TeachingStaff staff = new TeachingStaff(data[0],data[1],data[2],data[3],data[4],data[5]);
				input.add(staff);
				line = br.readLine();
			}
			
		}
		catch(Exception ex)
		{}
		ArrayList<TeachingStaff> output = loading.teachingStaff();
		
		String id1 = input.get(0).getId();
		String id2 = output.get(0).getId();
		
		assertEquals(id1, id2);
		
	}

	@Test
	public void testClasses() {
		LoadingData loading  = new LoadingData();
		ArrayList<Classes> input = new ArrayList<>();
		try
		{
			File file = new File("src\\classes.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while(line != null)
			{
				String[] data = line.split(",");
				Classes cls = new Classes(data[0], data[1], data[2], Integer.parseInt(data[3]), Integer.parseInt(data[4]), data[5]);
				input.add(cls);
				line = br.readLine();
			}
			
		}
		
		
		catch(Exception ex)
		{}
		ArrayList<Classes> output = loading.classes();
		
		String id1 = input.get(0).getId();
		String id2 = output.get(0).getId();
		
		assertEquals(id1, id2);
		
	}

}
