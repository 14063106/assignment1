
public class TeachingStaff {
	
	private String id;
	private String name;
	private String address;
	private String email;
	private String type;
	private String expertise;
	
	public TeachingStaff(String id, String name, String address, String email, String type, String expertise) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.email = email;
		this.type = type;
		this.expertise = expertise;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExpertise() {
		return expertise;
	}

	public void setExpertise(String expertise) {
		this.expertise = expertise;
	}
	
	
	
	

}
