import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;


public class Main extends JFrame implements ActionListener{

	
	private JLabel heading;
	private JButton button1,button2,button3, report,studentSubmit,researchButton, visitorButton;
	private JLabel id, name, email, researchLabel, visitorLabel, appointmentLabel;
	private JLabel studentSubject, expertiseSubject;
	private JTextField idText,nameText,emailText, researchText, visitorText, appointmentText;
	private JComboBox<String> subjectName,expertName;
	private JTextArea jTextArea;
	private JScrollPane pane;
	
	ArrayList<TeachingStaff> teachingStaffList = new ArrayList<>();
	ArrayList<Student> studentList = new ArrayList<>();
	ArrayList<Classes> classesList = new ArrayList<>();
	
	
	public Main()
	{
		LoadingData data = new LoadingData();
		teachingStaffList = data.teachingStaff();
		classesList = data.classes();
		init();
	}
	
	
	public void init()
	{
		
		setSize(1000,800);
		setLayout(null);
		setTitle("University Store Information");

		heading  = new JLabel("University Store Information");
		heading.setBounds(300, 10, 350, 40);
		heading.setFont(new Font("Arial",1,24));
		heading.setVisible(true);
		add(heading);
		
		button1 = new JButton("STUDENT");
		button1.setBounds(50,200,180,40);
		button1.setFont(new Font("Arial",1,18));
		button1.addActionListener(this);
		
		button2 = new JButton("RESEARCHER");
		button2.setBounds(50,280,180,40);
		button2.setFont(new Font("Arial",1,18));
		button2.addActionListener(this);
		
		button3 = new JButton("VISITORS");
		button3.setBounds(50,360,180,40);
		button3.setFont(new Font("Arial",1,18));
		button3.addActionListener(this);
		
		report = new JButton("REPORT");
		report.setBounds(50,440,180,40);
		report.setFont(new Font("Arial",1,18));
		report.addActionListener(this);
		
		id  = new JLabel("ID: ");
		id.setBounds(450, 200, 100, 40);
		id.setFont(new Font("Arial",1,18));
		id.setVisible(false);
		
		name  = new JLabel("Name: ");
		name.setBounds(450, 250, 100, 40);
		name.setFont(new Font("Arial",1,18));
		name.setVisible(false);
		
		email  = new JLabel("Email: ");
		email.setBounds(450, 300, 100, 40);
		email.setFont(new Font("Arial",1,18));
		email.setVisible(false);
		
		studentSubject  = new JLabel("Subject: ");
		studentSubject.setBounds(450, 350, 100, 40);
		studentSubject.setFont(new Font("Arial",1,18));
		studentSubject.setVisible(false);
		
		expertiseSubject  = new JLabel("Expertise ID: ");
		expertiseSubject.setBounds(450, 400, 130, 40);
		expertiseSubject.setFont(new Font("Arial",1,18));
		expertiseSubject.setVisible(false);
		
		idText  = new JTextField();
		idText.setBounds(620, 200, 100, 40);
		idText.setFont(new Font("Arial",1,18));
		idText.setVisible(false);
		
		nameText  = new JTextField();
		nameText.setBounds(620, 250, 100, 40);
		nameText.setFont(new Font("Arial",1,18));
		nameText.setVisible(false);
		
		emailText  = new JTextField();
		emailText.setBounds(620, 300, 100, 40);
		emailText.setFont(new Font("Arial",1,18));
		emailText.setVisible(false);
		
		subjectName  = new JComboBox<>(new String[]{"Computer Science","Business","Economics"});
		subjectName.setBounds(620, 350, 200, 40);
		subjectName.setFont(new Font("Arial",1,18));
		subjectName.setVisible(false);
		
		expertName  = new JComboBox<>(new String[]{"TS101","TS102","TS103","TS104"});
		expertName.setBounds(620, 400, 200, 40);
		expertName.setFont(new Font("Arial",1,18));
		expertName.setVisible(false);
		
		studentSubmit  = new JButton("Submit");
		studentSubmit.setBounds(620, 500, 150, 80);
		studentSubmit.setFont(new Font("Arial",1,18));
		studentSubmit.setVisible(false);
		studentSubmit.addActionListener(this);
		
		researchLabel  = new JLabel("Lecturer Name: ");
		researchLabel.setBounds(450, 200, 160, 40);
		researchLabel.setFont(new Font("Arial",1,18));
		researchLabel.setVisible(false);
		
		researchText  = new JTextField();
		researchText.setBounds(620, 200, 250, 40);
		researchText.setFont(new Font("Arial",1,18));
		researchText.setVisible(false);
		
		researchButton  = new JButton("Appointment");
		researchButton.setBounds(620, 280, 150, 40);
		researchButton.setFont(new Font("Arial",1,18));
		researchButton.setVisible(false);
		researchButton.addActionListener(this);
		
		visitorLabel  = new JLabel("Lecturer Name: ");
		visitorLabel.setBounds(450, 200, 160, 40);
		visitorLabel.setFont(new Font("Arial",1,18));
		visitorLabel.setVisible(false);
		
		visitorText  = new JTextField();
		visitorText.setBounds(620, 200, 250, 40);
		visitorText.setFont(new Font("Arial",1,18));
		visitorText.setVisible(false);
		
		
		appointmentLabel  = new JLabel("Appointment Timing: ");
		appointmentLabel.setBounds(450, 150, 180, 40);
		appointmentLabel.setFont(new Font("Arial",1,16));
		appointmentLabel.setVisible(false);
		
		appointmentText  = new JTextField();
		appointmentText.setBounds(620, 150, 250, 40);
		appointmentText.setFont(new Font("Arial",1,18));
		appointmentText.setVisible(false);
				
		visitorButton  = new JButton("Appointment");
		visitorButton.setBounds(620, 280, 150, 40);
		visitorButton.setFont(new Font("Arial",1,18));
		visitorButton.setVisible(false);
		visitorButton.addActionListener(this);
		
		jTextArea = new JTextArea();
		jTextArea.setFont(new Font("Arial",1,18));
		
		pane = new JScrollPane(jTextArea);
		pane.setBounds(400, 200, 500, 400);
		pane.setVisible(false);
		
		
		
		add(appointmentLabel);add(appointmentText);
		add(button1);add(button2);add(button3);
		add(id);add(name);add(email);add(studentSubject);add(expertiseSubject);
		add(idText);add(nameText);add(emailText);add(expertName);add(subjectName);add(studentSubmit);
		add(researchButton);add(researchText);add(researchLabel);
		add(visitorLabel);add(visitorText);add(visitorButton);
		add(report);add(pane);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}		
	
	
	public static void main(String[] args) {
	
		new Main().setVisible(true);;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == button1)
		{
			id.setVisible(true);
			name.setVisible(true);
			email.setVisible(true);
			studentSubject.setVisible(true);
			expertiseSubject.setVisible(true);
			
			idText.setVisible(true);
			nameText.setVisible(true);
			emailText.setVisible(true);
			expertName.setVisible(true);
			subjectName.setVisible(true);
			studentSubmit.setVisible(true);
						
			researchLabel.setVisible(false);
			researchText.setVisible(false);
			researchButton.setVisible(false);
			
			visitorLabel.setVisible(false);
			visitorText.setVisible(false);
			visitorButton.setVisible(false);
			
			appointmentLabel.setVisible(false);
			appointmentText.setVisible(false);
			pane.setVisible(false);
			
		}
		
		if(e.getSource() == button2)
		{
			id.setVisible(false);
			name.setVisible(false);
			email.setVisible(false);
			studentSubject.setVisible(false);
			expertiseSubject.setVisible(false);
			
			idText.setVisible(false);
			nameText.setVisible(false);
			emailText.setVisible(false);
			expertName.setVisible(false);
			subjectName.setVisible(false);
			studentSubmit.setVisible(false);
			
			researchLabel.setVisible(true);
			researchText.setVisible(true);
			researchButton.setVisible(true);
			
			visitorLabel.setVisible(false);
			visitorText.setVisible(false);
			visitorButton.setVisible(false);
			
			appointmentLabel.setVisible(true);
			appointmentText.setVisible(true);
			
			pane.setVisible(false);
		}
		
		if(e.getSource() == button3)
		{
			id.setVisible(false);
			name.setVisible(false);
			email.setVisible(false);
			studentSubject.setVisible(false);
			expertiseSubject.setVisible(false);
			
			idText.setVisible(false);
			nameText.setVisible(false);
			emailText.setVisible(false);
			expertName.setVisible(false);
			subjectName.setVisible(false);
			studentSubmit.setVisible(false);
			
			researchLabel.setVisible(false);
			researchText.setVisible(false);
			researchButton.setVisible(false);
			
			visitorLabel.setVisible(true);
			visitorText.setVisible(true);
			visitorButton.setVisible(true);
			
			appointmentLabel.setVisible(true);
			appointmentText.setVisible(true);
			
			pane.setVisible(false);
		}
		
		if(e.getSource() == studentSubmit)
		{
			researchLabel.setVisible(false);
			researchText.setVisible(false);
			researchButton.setVisible(false);
			
			String sid = idText.getText().trim();
			String sname = nameText.getText().trim();
			String semail = emailText.getText().trim();
			String sub = (String)subjectName.getSelectedItem();
			String exp = (String)expertName.getSelectedItem();
			
			if((sid.trim().length() == 0) || (sname.trim().length() == 0) || (semail.trim().length() <=10))
				JOptionPane.showMessageDialog(this,"Enter Valid Data","Sign Up",JOptionPane.INFORMATION_MESSAGE);
			else
			{
				Student s = new Student(sid, sname, semail, sub, exp);
				studentList.add(s);
				
				int ch = 0;
				for(Classes cls : classesList)
				{
					if(exp.equals(cls.getId()) && sub.equals(cls.getType()) && cls.getStudentNumber() >=1)
					{
						int totalStudent = cls.getStudentNumber() - 1;
						cls.setStudentNumber(totalStudent);
						ch = 1;
						break;
					}
				}
				if(ch == 1 )
					JOptionPane.showMessageDialog(this,"Successful","Student Sign Up",JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(this,"Unsuccessful, No seats avaliable","Sign Up",JOptionPane.INFORMATION_MESSAGE);
				id.setVisible(false);
				name.setVisible(false);
				email.setVisible(false);
				studentSubject.setVisible(false);
				expertiseSubject.setVisible(false);
				
				idText.setVisible(false);
				nameText.setVisible(false);
				emailText.setVisible(false);
				expertName.setVisible(false);
				subjectName.setVisible(false);
				studentSubmit.setVisible(false);
			}
		}
		
		if(e.getSource() == researchButton)
		{
			int check = 0;
			String name = researchText.getText().trim();
			if(name.trim().length() == 0 )
				JOptionPane.showMessageDialog(this, "Invalid name","Error",JOptionPane.ERROR_MESSAGE);
			else
			{
				int timing = Integer.parseInt(appointmentText.getText());
				if(timing <= 0 )
					JOptionPane.showMessageDialog(this, "Enter a Valid Time","Error",JOptionPane.ERROR_MESSAGE);
				else
				{
					for(TeachingStaff ts : teachingStaffList)
					{
						if(ts.getName().equalsIgnoreCase(name))
						{
							check = 1;
							break;
						}
					}
					int ch = 0;
					if(check == 1)
					{
						for(Classes ts : classesList)
						{
							if(ts.getTiming() == timing)
							{
								ch = 1;
								break;
							}
						}
						if(ch == 0)
							JOptionPane.showMessageDialog(this, "Reseacher Appointment Confirmed\nAppointment Timing: "+timing+":00","Appointment",JOptionPane.INFORMATION_MESSAGE);
						else
							JOptionPane.showMessageDialog(this, "Time slot unavaliable, Please enter another time for the appointment","Appointment",JOptionPane.ERROR_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(this, "Lecturer Not Found","Appointment",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
		if(e.getSource() == visitorButton)
		{
			int check = 0;
			String name = researchText.getText().trim();
			if(name.trim().length() == 0 )
				JOptionPane.showMessageDialog(this, "Name not valid","Error",JOptionPane.ERROR_MESSAGE);
			else
			{
				int timing = Integer.parseInt(appointmentText.getText());
				if(timing <= 0 )
					JOptionPane.showMessageDialog(this, "Enter Valid Timing","Error",JOptionPane.ERROR_MESSAGE);
				else
				{
					for(TeachingStaff ts : teachingStaffList)
					{
						if(ts.getName().equalsIgnoreCase(name))
						{
							check = 1;
							break;
						}
					}
					int ch = 0;
					if(check == 1)
					{
						for(Classes ts : classesList)
						{
							if(ts.getTiming() == timing)
							{
								ch = 1;
								break;
							}
						}
						if(ch == 0)
							JOptionPane.showMessageDialog(this, "Reseacher Appointment Confirmed\nAppointment Timing: "+timing+":00","Appointment",JOptionPane.INFORMATION_MESSAGE);
						else
							JOptionPane.showMessageDialog(this, "Busy, Please enter another time for the appointment","Appointment",JOptionPane.ERROR_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(this, "Lecturer Not Found","Appointment",JOptionPane.ERROR_MESSAGE);
				}
			}
			
		}
		
		if(e.getSource() == report)
		{
			id.setVisible(false);
			name.setVisible(false);
			email.setVisible(false);
			studentSubject.setVisible(false);
			expertiseSubject.setVisible(false);
			
			idText.setVisible(false);
			nameText.setVisible(false);
			emailText.setVisible(false);
			expertName.setVisible(false);
			subjectName.setVisible(false);
			studentSubmit.setVisible(false);
						
			researchLabel.setVisible(false);
			researchText.setVisible(false);
			researchButton.setVisible(false);
			
			visitorLabel.setVisible(false);
			visitorText.setVisible(false);
			visitorButton.setVisible(false);
			
			appointmentLabel.setVisible(false);
			appointmentText.setVisible(false);
			
			pane.setVisible(true);
			
			if(studentList.size() == 0)
			{
				JOptionPane.showMessageDialog(this, "No Students or Teaching Staff Found!","No Student",JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				jTextArea.setText("ID\tName\tEmail\t\tSubject\tLecturerID");
				jTextArea.append("\n==========================================================================\n\n");
				for(Student s: studentList)
				{
					jTextArea.append(s.getId()+"\t"+s.getName()+"\t"+s.getEmail()+"\t\t"+ s.getSubject()+"\t"+s.getExpertiseID()+"\n");
				}
				jTextArea.append("\n==========================================================================");
				
			}
			
		}
	}

}
