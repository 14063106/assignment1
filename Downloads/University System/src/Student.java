
public class Student {
	
	private String id;
	private String name;
	private String email;
	private String subject;
	private String expertiseID;

	
	public static final int count = 0;

	public Student(String id, String name, String email, String subject, String expertiseID) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.subject = subject;
		this.expertiseID = expertiseID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getExpertiseID() {
		return expertiseID;
	}

	public void setExpertiseID(String expertiseID) {
		this.expertiseID = expertiseID;
	}

	public static int getCount() {
		return count;
	}
	
	

} 
